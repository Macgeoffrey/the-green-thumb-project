[ESP32]: https://en.wikipedia.org/wiki/ESP32
[I/O]: https://en.wikipedia.org/wiki/Input/output
[DOIT ESP32 DevKit V1]: https://docs.zerynth.com/latest/official/board.zerynth.doit_esp32/docs/index.html
[Arduino IDE]: https://www.arduino.cc/en/main/software
[LICENSE.md]: /LICENSE.md
[microcontroller]: https://en.wikipedia.org/wiki/Microcontroller
[Fritzing]: http://fritzing.org/home/
[breadboard]: https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard/all 
[PCBs]: https://en.wikipedia.org/wiki/Printed_circuit_board
["Installing the ESP32 Board in Arduino IDE"]: https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/

# The Green Thumb Project :seedling:

The Green Thumb Project (GTP), in short, is an open-source greenhouse monitoring solution. 

This project provides everything necessary for making your own modular greenhouse monitoring kit, based around the [ESP32]. From wiring schematics, to coding, to 3D printed cases and more, we have covered it all. 

> *In this README, we will only be discussing the code/software. If you would like to learn more about the hardware, check out our website at [insert website here]*

__*This file is divided into several parts with subsections. Feel free to use this table of contents to help you navigate the project:*__

---
## Table of Contents
---

### 1. [How It Works](#How-It-Works)

* Decentralization
* Modules
* Databasing

### 2. [Modules](#Modules)
       
* Getting Started
* What is Arduino?
* What is an ESP32?
* Using the ESP32 with Arduino IDE
* Pushing the Code
* In-Depth Explanation of the Code
* The Hardware (3D Models and Wiring)

### 3. [Databasing](#Databasing)

* Working with Digital Ocean
* Basics of Databasing/How It Works
* What is a JSON String?
* POST & Request
* Setting up the Software (Docker)
* In-Depth Explanation of the Code
* The Hardware

### 4. [Contribution](#Contribution)

* Contribution Guidelines

### 5. [Licensing](#Licensing)

* CC-BY-SA License

---
# How It Works

**DOCUMENTATION IN PROGRESS**

---
# Modules

## Getting Started:

You've already learned that GTP consists of many "modules", each of which functions as an environmental sensor that records data to the web. This section serves as a guide on how to setup those modules, and discusses the code in-depth.

To start, the Green Thumb Project currently consists of 5 base modules:

***INSERT TABLE HERE***

GTP uses Arduino software to program it's modules. 

## What is Arduino?

Well, here's a brief introduction.

In the words of Arduino *(themselves?)*:

> Arduino is an open-source electronics platform based on easy-to-use hardware and software. Arduino boards are able to read inputs - light on a sensor, a finger on a button, or a Twitter message - and turn it into an output - activating a motor, turning on an LED, publishing something online. You can tell your board what to do by sending a set of instructions to the microcontroller on the board. To do so you use the Arduino programming language (based on Wiring), and the Arduino Software (IDE), based on Processing.

***Thanks Arduino!***

So basically, Arduino is both a software (Arduino IDE) and a programming language. Arduino, which is similar to C, is a very simple language to learn, and it is compatible with the [microcontroller] we will be using, so all of our code will be written in it.

*To learn more about Arduino, check out their [website](https://www.arduino.cc/).*

Speaking of microcontrollers, we will be using an [ESP32].

## What is an ESP32?

![Image of an ESP32](/Images/ESP32.jpg)

*(the CAD file shown above can be found [here].)*

In this project, we will be using the [ESP32] chip (more specifically, the [DOIT ESP32 DevKit V1]).

> *Please note that most ESP32 Boards will work with this project, but the CAD models may not be compatible. Experiment at your own risk!*

 Similar to the Arduino UNO, the ESP32 is a microcontroller that supports digital and analog [I/O]. It's programmed via MicroUSB, and sports Serial Communcation. However, the ESP32 stands out from the Arduino with its large variety of extra features, including built-in WiFi and Bluetooth!

Built-in WiFi allows us to communicate with a database, and gives us the ability to decentralize the entire system. Bluetooth allows us to talk to the devices with a smartphone, and opens the door for communication between modules. 

All of this, bundled into a cheap *(~10$)* and small form factor,  serves our purposes quite splendidly!

## Using the ESP32 with Arduino IDE

Lucky for us, the ESP32 is compatible with the Arduino IDE *(with a bit of finagling)*. In this section, we will show you how to setup your Arduino IDE to work with the ESP32, and also how to push our code to the ESP32!

The first thing you will want to do is install the [Arduino IDE].

*(It's not a very difficult installation process, so we will not be covering it here. If you need help installing it, try [this tutorial](http://www.circuitbasics.com/arduino-basics-installing-software/).)*

Once you have the software installed, you're also going to want to download the board we are using. Check out this tutorial for full instructions: ["Installing the ESP32 Board in Arduino IDE"]

And that's it! Now the Arduino IDE should be configured to work with the ESP32!

## Pushing Code to the ESP32

Pushing Code to the ESP32 is the exact same as the pushing to any other microcontroller. 

First, you will want to plug in the ESP32 via MicroUSB to your computer.

Make sure the board is set to DOIT ESP32 DevKit V1. *(This can be set by going to Tools>Board>DOIT ESP32 DEVKIT V1)*

Click the :heavy_check_mark: Checkmark Icon in the top left to verify the code, and then if that finishes successfully, click the :arrow_right: Upload Icon. This will push the code to the ESP32.

> Note: **The ESP32 will NOT recieve code unless you hold down the "BOOT" button. This is an error with driver versions, so if you have trouble pushing to the board, try this fix:** *Once you see the the line, "Connecting......____...", Hold down the BOOT button and* ***Voila!***

Once the code is uploaded you should see the line:

```
Writing at 0x00008000... (100 %)
Wrote 3072 bytes (144 compressed) at 0x00008000 in 0.0 seconds (effective 877.7 kbit/s)...
Hash of data verified.

Leaving...
Hard resetting via RTS pin...
```

To confirm the code is working, we can go to Tools>Serial Monitor, and we should be seeing an output similar to this:

```
Time:12369
Temperature:22.7C
Humidity:34.3%
String Length: 44
Connecting...
POSTED!!!
```
If you have something similar to this, then you have successfully programmed your ESP32!

***Congratulations!*** :tada:

## In-Depth Explanation of the Code

**DOCUMENTATION IN PROGRESS**

## The Hardware

> Note: This file will **not** cover how to build the modules themselves, but we would like to clarify a few things. 

First of all, GTP uses a circuit program called [Fritzing].

This open-source software lets you virtually [breadboard] and test electronics, as well as design schematics and even custom [PCBs]. We will be using Fritzing to create all of our wiring diagrams. All wiring files are included in this repository. Fritzing files end with .fzz, and they can be located in the Fritzing folder. 

*(We will not be covering the installation of Fritzing, but here is a helpful [tutorial](http://fritzing.org/download/#install).)*

Secondly, GTP uses Fusion360 to design 3D-printed cases for each module, the files of which are available on our website at [insert website here]. 

##### ***(Credit to Daniel Barbosa for his amazing work!)***

Lastly, GTP has provided full documentation for wiring and building these modules on our website, [insert website here].

---
# Databasing

**DOCUMENTATION IN PROGRESS**

---
# Contribution

**DOCUMENTATION IN PROGRESS**

---
# Licensing

The Green Thumb Project is licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). 

![CC BY-SA 4.0 License Image](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

To learn more, please consult the [LICENSE.md] file.