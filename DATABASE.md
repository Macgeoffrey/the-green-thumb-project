Welcome to the database documentation!

TL;DR Database setup:

    docker setup
    clone repository  
    Digital ocean setup

How it works:

    Module communicates with database
    database recording information
    Web Interface

Indepth Code Breakdown:

    python, no SQL
    two html pages
    uses Vue.js
    
Contributions:

    Security issues
    Licensing
    UI update
    Offline version
    Graphing
    App?
    how to contribute (link to site)