//Include Libraries
#include "WiFi.h"
#include "SparkFunHTU21D.h"
#include "ArduinoJson.h"

//Set WiFi Network and Password
#define WIFI_SSID "McWifi"
#define WIFI_PASS "letsdance"

//Set Variables
HTU21D myHumidity;

WiFiClient client;

String header;
 
void setup(void)
{
 
  //Setup Serial and Sensor
  Serial.begin(9600);
  myHumidity.begin();
  
  while (!Serial);
  Serial.println("Serial & Sensor initialized.");
  Serial.print("Connecting to ");
  Serial.print(WIFI_SSID);

  //Connect to WiFi
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  
  // Blink LED twice per second while waiting for connection
  pinMode(LED_BUILTIN, OUTPUT);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN,HIGH);
    delay(250);
    digitalWrite(LED_BUILTIN,LOW);
    delay(250);
    Serial.print("."); 
  }

  //Print Success Mesage
  Serial.println("");
  Serial.print("WiFi (");
  Serial.print(WiFi.macAddress());
  Serial.print(") connected with IP ");
  Serial.println(WiFi.localIP());
  Serial.print("DNS0 : ");
  Serial.println(WiFi.dnsIP(0));
  Serial.print("DNS1 : ");
  Serial.println(WiFi.dnsIP(1));
}

void loop(void)
{
  float humd = myHumidity.readHumidity();
  float temp = myHumidity.readTemperature();
 
  //Print enviromental readings
  Serial.print("Time:");
  Serial.println(millis());
  Serial.print("Temperature:");
  Serial.print(temp, 1);
  Serial.println("C");
  Serial.print("Humidity:");
  Serial.print(humd, 1);
  Serial.println("%");
  
  //Serialize environmental data in JSON
  StaticJsonBuffer<300> jsonBuffer;
  JsonObject& jsonEncoder = jsonBuffer.createObject();
  jsonEncoder["sensor_id"] = "7";
  jsonEncoder["token"] = "aECv1meaIIZMe5tCgtC";
  jsonEncoder["reading"] = temp;
  jsonEncoder["reading"] = humd;

   //Print String Length
  Serial.print("String Length: ");
  Serial.println(jsonEncoder.measureLength());
  
  //Attempt Web Connection
  char web_server[] = "www.jandbri.com";
  if (client.connect(web_server, 80)) {
    Serial.println("Connecting...");
    // send the HTTP POSTT request:
    client.println("POST /api/ HTTP/1.1");
    client.println("Host: IP_OF_WEBSERVER");
    client.println("User-Agent: ESP32WiFi/1.1");
    client.print("Content-Length: ");
    client.println(jsonEncoder.measureLength());
    client.println("Connection: close");
    client.println();
    jsonEncoder.printTo(client);
    client.stop();
    Serial.println("POSTED!!!");
  } else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
  }

  //Set A Delay
  delay(10000);
}
