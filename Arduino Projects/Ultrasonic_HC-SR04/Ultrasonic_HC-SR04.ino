//Include WiFi and Json Libraries
#include "WiFiClientSecure.h"
#include "ArduinoJson.h"

//Set WiFi Network and Password
const char* ssid     = "Mobile McWifi";     // your network SSID (name of wifi network)
const char* password = "letsdance"; // your network password

const char*  server = "165.22.33.33";  
const int  port = 443;

const char* root_ca= \
    "-----BEGIN CERTIFICATE-----\n" \
    "MIIENTCCAx2gAwIBAgIUYmhg1V41UknwHhCJA71I+WrgVVgwDQYJKoZIhvcNAQEL\n" \
    "BQAwgakxCzAJBgNVBAYTAlVTMQ8wDQYDVQQIDAZLYW5zYXMxFDASBgNVBAcMC0th\n" \
    "bnNhcyBDaXR5MRwwGgYDVQQKDBNHcmVlbiBUaHVtYiBQcm9qZWN0MRMwEQYDVQQL\n" \
    "DApEYXRhYmFzaW5nMRIwEAYDVQQDDAlsb2NhbGhvc3QxLDAqBgkqhkiG9w0BCQEW\n" \
    "HWdyZWVudGh1bWJwcm9qZWN0MThAZ21haWwuY29tMB4XDTE5MDcxMjE1MjMyNVoX\n" \
    "DTIyMDcxMTE1MjMyNVowgakxCzAJBgNVBAYTAlVTMQ8wDQYDVQQIDAZLYW5zYXMx\n" \
    "FDASBgNVBAcMC0thbnNhcyBDaXR5MRwwGgYDVQQKDBNHcmVlbiBUaHVtYiBQcm9q\n" \
    "ZWN0MRMwEQYDVQQLDApEYXRhYmFzaW5nMRIwEAYDVQQDDAlsb2NhbGhvc3QxLDAq\n" \
    "BgkqhkiG9w0BCQEWHWdyZWVudGh1bWJwcm9qZWN0MThAZ21haWwuY29tMIIBIjAN\n" \
    "BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtJjCt/aeRrbYGBltjuL6SHToSg9y\n" \
    "DyqH1JYoprTXAfyIwp4KiCk+0vm+lTCEyefTyXItKT8DcW3nNUBUsu/MNJ7oO+4y\n" \
    "VtfVEybjoqgHyAXQ9+W5FFMwIOXRXNcCmQK6u76v5knXiQHEp7+Jgfz/Ha80iJ22\n" \
    "LvJLJiyPKeWoQyRmUHpdpQXtzD0C328iODBEewxJ/rtpIgTcIXeaFlbseJpTRyH4\n" \
    "b1MfYq1XIgAfZomJnih+DPeeousMdTegZBBAXUhR392FcIkNCn5jm8xK9JbXe0rj\n" \
    "W9pr3+Ch9gzQ0WsBuHV7uSmwEDQSMh/oukiv9sG7dFPwTcvsqEzHZbJCMwIDAQAB\n" \
    "o1MwUTAdBgNVHQ4EFgQURTQ8DBvjcoHYyCaagri2vgBgMwcwHwYDVR0jBBgwFoAU\n" \
    "RTQ8DBvjcoHYyCaagri2vgBgMwcwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0B\n" \
    "AQsFAAOCAQEApky28fpxFbPWbZSgy43YrIcaRby6F6EgBOaGqzrb+vQo9aM16gOh\n" \
    "+bibgEtZzedPX6teSIa4mLJhxJ4VRhjNVihxp2e9uedb1uaJ6n7zWYjqBcgc7xPd\n" \
    "wMESiDyhL1+4v9B0gIiuUbtMnJDZHCtIIKuQtMStt2+LCDTCL1JhZZ2PEOTOxAWP\n" \
    "Myr7GPR549aVZA6CNOIjnr4r92g4KfEnLli6R18i5RbxuxFUig0lvdra1A3Rjv4V\n" \
    "LUR32mQkFSLw/0IwXTqwvG1fgujRZH9yiQyCm7mzJp2eoXikzTvqcdPKHnABPEHG\n" \
    "AZqELoiQLeLrVZKvZRlXLoJL1mhH3PKMvw==\n" \
    "-----END CERTIFICATE-----\n";

//Set Database Sensor Values
int sensor_id = 2;
String token = "rYzbUqfJwTdRjZvG";

//Set Variables
long duration;
float distance;

//Set Sensor Pins
const int trigPin = 2;
const int echoPin = 5;

WiFiClientSecure client;
 
void setup()
{
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  delay(100);

  Serial.println("Serial & Sensor initialized.");
  Serial.print("Attempting to connect to SSID: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  // attempt to connect to Wifi network:
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    // wait 1 second for re-trying
    delay(1000);
  }

  //Print Success Message
  Serial.println("");
  Serial.print("WiFi (");
  Serial.print(WiFi.macAddress());
  Serial.print(") connected with IP ");
  Serial.println(WiFi.localIP());
  Serial.print("DNS0 : ");
  Serial.println(WiFi.dnsIP(0));
  Serial.print("DNS1 : ");
  Serial.println(WiFi.dnsIP(1));

  //Setup Pins
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
}

void loop(void)
{
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);

  // Calculating the distance
  distance = duration*0.0133/2;

  // Prints Readings
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println("in");

  //Serialize environmental data in JSON
  StaticJsonBuffer<300> jsonBuffer;
  JsonObject& jsonEncoder = jsonBuffer.createObject();
  jsonEncoder["sensor_id"] = sensor_id;
  jsonEncoder["token"] = token;
  jsonEncoder["reading"] = distance;

  //Print String Length
  Serial.print("String Length: ");
  Serial.println(jsonEncoder.measureLength());

  //Attempting to Connect to Server
  Serial.println("\nStarting connection to server...");
  if (!client.connect(server, port)){
    Serial.println("Connection failed!");
  } else {
    Serial.println("Connected to server!");
    //Make an HTTP POST:
    client.println("POST /api/ HTTP/1.1");
    client.println("Host: https://");
    client.print(server);
    client.println("User-Agent: ESP32WiFi/1.1");
    client.print("Content-Length: ");
    client.println(jsonEncoder.measureLength());
    client.println("Connection: close");
    client.println();
    jsonEncoder.printTo(client);
    client.stop();
    Serial.println("POSTED!!!");
  }

  //Set A Delay
  delay(10000);
}
