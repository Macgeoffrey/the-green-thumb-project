//Include Libraries
#include "WiFi.h"
#include "Wire.h"
#include "DHT.h"
#include "ArduinoJson.h" 

//Set WiFi Network and Password
#define WIFI_SSID "McWifi"
#define WIFI_PASS "letsdance"

//Set Variables
HTU21D myHumidity;

WiFiClient client;

#define DHTTYPE DHT11
#define DHTPIN 15

DHT dht(DHTPIN, DHTTYPE);

String header;
 
void setup(void)
{
 
  //Setup Serial and Sensor
  Serial.begin(9600);
  dht.begin();
  
  while (!Serial);
  Serial.println("Serial & Sensor initialized.");
  Serial.print("Connecting to ");
  Serial.print(WIFI_SSID);

  //Connect to WiFi
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  
  // Blink LED twice per second while waiting for connection
  pinMode(LED_BUILTIN, OUTPUT);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN,HIGH);
    delay(250);
    digitalWrite(LED_BUILTIN,LOW);
    delay(250);
    Serial.print("."); 
  }

  //Print Success Mesage
  Serial.println("");
  Serial.print("WiFi (");
  Serial.print(WiFi.macAddress());
  Serial.print(") connected with IP ");
  Serial.println(WiFi.localIP());
  Serial.print("DNS0 : ");
  Serial.println(WiFi.dnsIP(0));
  Serial.print("DNS1 : ");
  Serial.println(WiFi.dnsIP(1));

  //Start Web Server
  server.begin();
}
 
void loop(void)
{
  float humd = dht.readHumidity();
  float temp = dht.readTemperature(true); //true means Fahrenheit

  Serial.print("Time:");
  Serial.print(millis());
  Serial.print(" Temperature:");
  Serial.print(temp);
  Serial.print("C");
  Serial.print(" Humidity:");
  Serial.print(humd);
  Serial.print("%");
  
  //Serialize environmental data in JSON
  StaticJsonBuffer<300> jsonBuffer;
  JsonObject& jsonEncoder = jsonBuffer.createObject();
  jsonEncoder["Temperature"] = temp;
  jsonEncoder["Humidity"] = humd;

  
  //Attempt Web Connection
  char web_server[] = "www.jandbri.com";
  if (client.connect(web_server, 80)) {
    Serial.println("Connecting...");
    // send the HTTP POSTT request:
    client.println("POST /SENSOR_NAME/SENSOR_NUMBER/ HTTP/1.1");
    client.println("Host: IP_OF_WEBSERVER");
    client.println("User-Agent: ESP32WiFi/1.1");
    client.print("Content-Length:" jsonEncoder.size());
    client.println("Connection: close");
    client.println();
    jsonEncoder.printTo(client);
    client.stop();
    Serial.println("POSTED!!!");
  } else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
  }

  //Set A Delay
  delay(10000);
}
